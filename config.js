module.exports = {
    'name'  : 'Blog 9',
    'camel' : 'Blog9',
    'slug'  : 'blog-9',
    'dob'   : 'Blog_9_1440',
    'desc'  : 'Blog posts in columns as full images overlayed with title and post meta.',
}