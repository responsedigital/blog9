class Blog9 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.$ = $(this)
        this.props = this.getInitialProps()
        this.resolveElements()
      }
    
    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse($('script[type="application/json"]', this).text())
        } catch (e) {}
        return data
    }

    resolveElements () {

    }

    connectedCallback () {
        this.initBlog9()
    }

    initBlog9 () {
        const { options } = this.props
        const config = {

        }

        // console.log("Init: Blog 9")
    }

}

window.customElements.define('fir-blog-9', Blog9, { extends: 'div' })
