<?php

namespace Fir\Pinecones\Blog9;

use Fir\Utils\GlobalFields;

class Schema
{
    public static function getACFLayout()
    {
        return [
            'name' => 'Blog9',
            'label' => 'Pinecone: Blog 9',
            'sub_fields' => [
                [
                    'label' => 'General',
                    'name' => 'generalTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Overview',
                    'name' => 'overview',
                    'type' => 'message',
                    'message' => "Blog posts in columns as full images overlayed with title and post meta."
                ],
                GlobalFields::getGroups(array(
                    'priorityGuides',
                    'ID'
                )),
                [
                    'label' => 'Options',
                    'name' => 'optionsTab',
                    'type' => 'tab',
                    'placement' => 'top',
                    'endpoint' => 0
                ],
                [
                    'label' => '',
                    'name' => 'options',
                    'type' => 'group',
                    'layout' => 'row',
                    'sub_fields' => [
                        GlobalFields::getOptions(array(
                            'numColumns',
                            'hideComponent'
                        ))
                    ]
                ]
            ]
        ];
    }
}
