<!-- Start Blog 9 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : Blog posts in columns as full images overlayed with title and post meta. -->
@endif
<div class="blog-9"  is="fir-blog-9">
  <script type="application/json">
      {
          "options": @json($options)
      }
  </script>
  <div class="blog-9__wrap {{ $classes }}">
    @for($x = 0; $x < $num_columns; $x++)
      <a class="blog-9__post" href="#">
        <img class="blog-9__post-img" src="{{ $post['featured_image'] ?: 'https://res.cloudinary.com/response-mktg/image/upload/q_auto,f_auto/v1594221943/fir/demos/img-placeholder.jpg' }}" alt="Placeholder">
        <div class="blog-9__post-overlay">
          <h5 class="blog-9__post-tag">{{ $post['tag'] ?: $faker->word }}</h5>
          <h4 class="blog-9__post-title">{{ $post['title'] ?: $faker->text($maxNbChars = 20) }}</h4>
          <span class="blog-9__post-meta">
            <p class="blog-9__post-author">{{ $post['author'] ?: $faker->name }}</p>
            <p class="blog-9__post-date">{{ $post['date'] ?: $faker->dayOfMonth($max = 'now') . ' ' . $faker->monthName($max = 'now')  . ' ' . $faker->year($max = 'now') }}</p>
          </span>
        </div>
      </a>
    @endfor
  </div>
</div>
<!-- End Blog 9 -->
